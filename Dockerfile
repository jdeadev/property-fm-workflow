FROM ubuntu:18.04

RUN useradd -ms /bin/bash frappe \
    && mkdir -p /frappe-bench/apps

WORKDIR /frappe-bench/apps
COPY --chown=frappe:frappe . /frappe-bench/apps

CMD [ "tail", "-f", "/dev/null" ]
