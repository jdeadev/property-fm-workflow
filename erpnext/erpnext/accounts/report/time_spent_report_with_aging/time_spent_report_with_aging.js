// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Time Spent Report With Aging"] = {
    filters: [
        {
            fieldname: "doctype",
            label: __("DocType"),
            fieldtype: "Link",
            options: "DocType",
            reqd: 1,
            get_query: () => {
                return {
                    query: "erpnext.accounts.report.time_spent_report.time_spent_report.get_doctype"
                }
            },
            on_change: function() {
                frappe.query_report.set_filter_value('to_status', null);
            }
        },
        {
            fieldname: "to_status",
            label: __("To Status"),
            fieldtype: "Link",
            options: "Workflow State",
            reqd: 1,
            get_query: () => {
                return {
                    query: "erpnext.accounts.report.time_spent_report.time_spent_report.get_status",
                    filters: {
                        doctype: frappe.query_report.get_filter_value('doctype'),
                        without_closed_cancelled: 1,
                    }
                }
            },
        },
        {
            fieldname: "date_created",
            label: __("Date Created"),
            fieldtype: "Date",
            reqd: 1,
        },
        {
            fieldname: "date_period",
            label: __("Date Period"),
            default: 1,
            fieldtype: "Int",
            reqd: 1,
        },
    ],
};