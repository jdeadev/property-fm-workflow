# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from itertools import groupby
from frappe.utils import cint

def execute(filters=None):
    columns, data = get_columns(filters), get_data(filters)
    return columns, data

def get_columns(filters):
    doctype_map = frappe.db.get_list('Time Spent Report Details', 
        filters={ 'ref_doctype': filters.get('doctype') }, 
        fields=['workflow_field', 'from_status', 'to_status']
    )
    doctype_map = doctype_map[0]

    return [
        {
            "fieldname": "name",
            "label": _("Document ID"),
            "fieldtype": "Link",
            "options": filters.get('doctype'),
            "width": 150
        },
        {
            "fieldname": "create_date",
            "label": _("Created Date"),
            "fieldtype": "Date",
        },
        # {
        #     "fieldname": "changed_date",
        #     "label": _("Changed Date"),
        #     "fieldtype": "Date",
        # },
        # {
        #     "fieldname": "status",
        #     "label": _("Status"),
        #     "fieldtype": "Data",
        #     "width": 200
        # },
        # {
        #     "fieldname": "to_status",
        #     "label": _("To Status"),
        #     "fieldtype": "Data",
        #     "width": 200
        # },
        # {
        #     "fieldname": "changer",
        #     "label": _("Changer"),
        #     "fieldtype": "Data",
        #     "width": 200
        # },
        # {
        #     "fieldname": "date_period",
        #     "label": _("Date Period"),
        #     "fieldtype": "Int",
        # },
        {
            "fieldname": "from_status_date",
            "label": _(f"""{doctype_map['from_status']} Date"""),
            "fieldtype": "Date",
        },
        {
            "fieldname": "to_status_date",
            "label": _(f"""{doctype_map['to_status']} Date"""),
            "fieldtype": "Date",
        },
        {
            "fieldname": "date_diff",
            "label": _("Date Diff"),
            "fieldtype": "Int",
        },
    ]

def get_data(filters):
    doctype = filters.get('doctype')
    # status = filters.get('status', 'All')
    # to_status = filters.get('to_status', 'All')
    # date_period = cint(filters.get('date_period'), 0)
    date_diff_period = cint(filters.get('date_diff_period'), 0)

    doctype_map = frappe.db.get_list('Time Spent Report Details', 
        filters={ 'ref_doctype': doctype }, 
        fields=['workflow_field', 'from_status', 'to_status']
    )
    doctype_map = doctype_map[0]

    doc = frappe.db.sql(f"""
        select
            doc.name,
            DATE(doc.creation) 'create_date',
            DATE(ver.modified) 'changed_date',
            JSON_UNQUOTE(json_extract(ver.data, '$.changed[0][1]')) 'status',
            JSON_UNQUOTE(json_extract(ver.data, '$.changed[0][2]')) 'to_status',
            ver.owner 'changer',
            DATEDIFF(ver.modified, doc.creation) 'date_period'
        from `tab{doctype}` doc
        left join `tabVersion` ver on doc.name = ver.docname
        where ver.ref_doctype = '{doctype}'
        and json_extract(ver.data, '$.changed[0][0]') = '{doctype_map['workflow_field']}'
        order by doc.name asc, ver.modified asc
    """, as_dict=True)

    for key, group in groupby(doc, lambda x: x.name):
        res = { 'from_status_date': None, 'to_status_date': None }
        for d in group:
            if not res.get('from_status_date') and d.to_status == doctype_map['from_status']:
                res.update({ 'from_status_date': d.changed_date })

            if not res.get('from_status_date') and d.status == doctype_map['from_status']:
                res.update({ 'from_status_date': d.changed_date })
                
            if not res.get('to_status_date') and d.to_status == doctype_map['to_status']:
                res.update({ 'to_status_date': d.changed_date })

            date_diff = frappe.utils.date_diff(
                res.get('to_status_date', frappe.utils.getdate()),
                res.get('from_status_date', frappe.utils.getdate())
            )
            d.update({
                'from_status_date': res.get('from_status_date'),
                'to_status_date': res.get('to_status_date'),
                'date_diff': date_diff
            })

    # doc = list({ v.name: v for v in doc if v.date_period >= 7 }.values())
    doc = list({ v.name: v for v in doc }.values())

    date_period_list = get_date_period()
    date_period_list = [int(x) for x in date_period_list.split("\n")]
    # date_period_index = date_period_list.index(date_period)
    # next_date_period = date_period_list[date_period_index + 1] \
    #     if date_period_list[-1] != date_period else date_period_list[-1]
    
    date_diff_period_index = date_period_list.index(date_diff_period)
    next_date_diff_period = date_period_list[date_diff_period_index + 1] \
        if date_period_list[-1] != date_diff_period else date_period_list[-1]

    doc_filter = []
    # for d in doc:
    #     if status == 'All' or (d.status == status and d.to_status == to_status):
    #         if date_period != next_date_period or date_diff_period != next_date_diff_period:
    #             if (d.date_period >= date_period and d.date_period <= next_date_period) and \
    #                 (d.date_diff >= date_diff_period and d.date_diff <= next_date_diff_period):
    #                 doc_filter.append(d)
    #         else:
    #             if d.date_period >= date_period and d.date_diff >= date_diff_period:
    #                 doc_filter.append(d)

    for d in doc:
        if d.date_diff >= date_diff_period and d.date_diff <= next_date_diff_period:
            doc_filter.append(d)
        if date_diff_period == next_date_diff_period and d.date_diff >= date_diff_period:
            doc_filter.append(d)
            
    return doc_filter

@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_doctype(doctype, txt, searchfield, start, page_len, filters, as_dict=False):
    doctype = []
    doctype = frappe.db.sql("""
        select ref_doctype 'name', ref_doctype
        from `tabTime Spent Report Details`
    """, as_dict=as_dict)

    return doctype

@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_status(doctype, txt, searchfield, start, page_len, filters, as_dict=False):
    status = []
    sql = ''
    if filters.get('doctype'):
        sql = f""" and wf.document_type = '{filters.get('doctype')}'"""

    status = frappe.db.sql(f"""
        select wds.state 'name', wf.document_type
        from `tabWorkflow Document State` wds
        left join `tabWorkflow` wf on wds.parent = wf.name
        where wf.is_active = 1 {sql} and wds.state like '%{txt}%'
        order by wds.idx asc
    """, as_dict=as_dict)

    return status

@frappe.whitelist()
def get_date_period():
    date_period = frappe.db.get_single_value('Time Spent Report Settings', 'date_period')
    return date_period

@frappe.whitelist()
def get_from_to_status(doctype):
    return frappe.db.sql(f"""
        select from_status, to_status
        from `tabTime Spent Report Details`
        where ref_doctype = '{doctype}'
    """, as_dict=True)[0]