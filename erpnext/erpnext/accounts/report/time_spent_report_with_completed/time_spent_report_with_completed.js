// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Time Spent Report With Completed"] = {
    filters: [
        {
            fieldname: "doctype",
            label: __("DocType"),
            fieldtype: "Link",
            options: "DocType",
            reqd: 1,
            get_query: () => {
                return {
                    query: "erpnext.accounts.report.time_spent_report.time_spent_report.get_doctype"
                }
            },
            on_change: function() {
                frappe.call({
                    method: "erpnext.accounts.report.time_spent_report.time_spent_report.get_from_to_status",
                    args: { doctype: frappe.query_report.get_filter_value('doctype') },
                    callback: function(r) {
                        console.log(r.message);
                        if (r.message) {
                            var label = `${r.message.from_status} To ${r.message.to_status}`;
                            frappe.query_report.set_filter_value('date_diff_period_label', label);

                            var f = frappe.query_report.get_filter('date_diff_period_label');
                            $(f.input_area).attr('data-original-title', label);
                        }
                    }
                });
            }
        },
        {
            fieldname: "date_diff_period_label",
            label: "",
            fieldtype: "Data",
            read_only: 1,
        },
        {
            fieldname: "date_diff_period",
            label: __("Date Diff Period"),
            fieldtype: "Select",
            options: '7\n14\n30',
            reqd: 1,
        },
    ],
    onload: function() {
		return  frappe.call({
			method: "erpnext.accounts.report.time_spent_report.time_spent_report.get_date_period",
			callback: function(r) {
                var date_diff_period = frappe.query_report.get_filter('date_diff_period');
				date_diff_period.df.options = r.message;
				date_diff_period.df.default = r.message.split("\n")[0];
				date_diff_period.refresh();
				date_diff_period.set_input(date_diff_period.df.default);
			}
		});
	}
};