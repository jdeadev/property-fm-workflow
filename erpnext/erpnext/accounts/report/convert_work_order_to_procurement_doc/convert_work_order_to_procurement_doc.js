frappe.query_reports["Convert Work Order to Procurement Doc"] = {
    "filters": [
        // {
        //     "fieldname": "from_date",
        //     "label": __("From Date"),
        //     "fieldtype": "Date",
        //     "reqd": 1, //Required
        //     "default": frappe.defaults.get_user_default("year_start_date"),
        // },
        // {
        //     "fieldname": "to_date",
        //     "label": __("To Date"),
        //     "fieldtype": "Date",
        //     "reqd": 1, //Required
        //     "default": frappe.defaults.get_user_default("year_end_date"),
        // },
        {
            "fieldname": "procurement_department",
            "label": __("Procurement Department"),
            "fieldtype": "Link",
            "options": "Department",
        },
        {
            "fieldname": "user_wo",
            "label": __(" Procurement Person"),
            "fieldtype": "Link",
            "options": "User",
        },
        {
            "fieldname": "vendor_code",
            "label": __("Vendor Code"),
            "fieldtype": "Link",
            "options": "Vendor",
        },
    ],
    get_datatable_options(options) {
		checkbox_column = Object.assign(options, {
            checkboxColumn: true,
            events: {
				onCheckRow: function(data) {
                    total_amount_selected()
                }
            }
        });
        return checkbox_column
    },
    onload: function(){
        // let $btn = page.set_primary_action('New', () => create_new(), 'octicon octicon-plus')
        if ( frappe.user.has_role("End user_2 FM Finance")) {
            frappe.query_report.page.set_primary_action('Convert', () => create_procurement_doc())
        }
    }
}

function total_amount_selected() {
    var data_selected = [];
    let element = $('#page-query-report > div.container.page-body > div.page-wrapper > div > div.row.layout-main > div > div.layout-main-section.frappe-card > div.report-wrapper > div > div.dt-footer > div > div.dt-cell.dt-cell--col-9 > div.dt-cell__content.dt-cell__content--col-9 > div')
    let total_all_row = 0.00
    let total_wo_amount = 0.00;
    $('.dt-scrollable').find(":input[type=checkbox]").each((idx, row) => {
        var row_index = $(row).parent().parent().attr('data-row-index')
        let wo_amount = frappe.query_report.data[row_index]['wo_amount'];
        total_all_row += wo_amount;
        if(row.checked) {
            data_selected.push(wo_amount)
            total_wo_amount += wo_amount;
        }
    });
    // if (data_selected.length >= 1) {element.text('฿ '+ (Number(total_wo_amount).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') )}
    // else {element.text('฿ '+ (Number(total_all_row).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') )}
    element.text('฿ '+ (Number(total_wo_amount).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') )
    return data_selected
}

function get_selected() {
    let report = frappe.query_report;
    let checked_rows_indexes = report.datatable.rowmanager.getCheckedRows();
    let checked_rows = checked_rows_indexes.map(i => report.data[i]);
    // let checked_rows = checked_rows_indexes.map(i => {
    //     // report.data[i].vendor_name = unescape(encodeURIComponent(report.data[i].vendor_name));
    //     return { name: report.data[i].name }
    // });
    return checked_rows
}

function create_procurement_doc(){
    data_set = get_selected()

    if (data_set.length > 0) {
        frappe.call({
            method: 'erpnext.accounts.report.convert_work_order_to_procurement_doc.convert_work_order_to_procurement_doc.convert_work_order',
            args: {'list_data': data_set},
            callback: function(r) {
                if(!r.exc) {
                    // frappe.model.sync(r.message);
                    // frappe.set_route("Form", r.message.doctype, r.message.name);
                    frappe.msgprint({
                        title: __('Notification'),
                        message: 'Create Procurement data successly',
                    });
                    frappe.query_report.refresh()
                }
            }
        });
    } else {
        frappe.msgprint({
            title: __('Notification'),
            message: "Doesn't has select item for convert.",
        });
    }

}