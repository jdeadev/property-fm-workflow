from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.mapper import get_mapped_doc

def execute(filters=None):
    columns, data = get_columns(), get_data(filters)
    return columns, data

def get_data(filters):
    q = frappe.db.sql("""
        select 
            name,
            pm_work_order_state,
            project_code_wo,
            work_order_code,
            work_order,
            format,
            vendor as vendor,
            vendor_name,
            procurement_department,
            user_wo,
            wo_amount
        from `tabVerisae Work Order`
        where 
            pm_work_order_state = 'Approved'
            {0}
        order by vendor, vendor_name
    """.format(get_condition(filters)), as_dict=1)
    return q

def get_condition(filters):
    condition = []
    if filters.get('vendor_code'):
        condition.append("vendor LIKE '%{0}%' ".format(filters.get('vendor_code')) )
    if filters.get('procurement_department'):
        condition.append("procurement_department LIKE '%{0}%' ".format(filters.get('procurement_department')) )
    if filters.get('user_wo'):
        condition.append("user_wo LIKE '%{0}%' ".format(filters.get('user_wo')) )
    return " and " + " and ".join(condition) if condition else ""

def get_columns():
    columns = [
        {
            "fieldname": "name",
            "label": _("Verisae Work Order"),
            "fieldtype": "Link",
            "options": "Verisae Work Order",
            "width": 150
        },
        # {
        #     "fieldname": "pm_work_order_state",
        #     "label": _("State"),
        #     "fieldtype": "Data",
        #     "width": 100
        # },
        # {
        #     "fieldname": "work_order_code",
        #     "label": _("Work Order Code"),
        #     "fieldtype": "Data",
        #     "width": 120
        # },
        {
            "fieldname": "project_code_wo",
            "label": _("Project Code"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "work_order",
            "label": _("Work Order"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "format",
            "label": _("Format"),
            "fieldtype": "Data",
            "width": 60
        },
        {
            "fieldname": "vendor",
            "label": _("Vendor Code"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "vendor_name",
            "label": _("Vendor"),
            "fieldtype": "Data",
            "width": 170
        },
        {
            "fieldname": "procurement_department",
            "label": _("Department"),
            "fieldtype": "Data",
            "width": 100
        },
        {
            "fieldname": "user_wo",
            "label": _("Procurement Person"),
            "fieldtype": "Data",
            "width": 170
        },
        {
            "fieldname": "wo_amount",
            "label": _("WO Amount"),
            "fieldtype": "Currency",
            "width": 170
        },
        
    ]

    return columns

@frappe.whitelist()
def convert_work_order(list_data):
    get_data = frappe.parse_json(list_data)
    group_by_vendor = {}
    for wk_name in get_data:
        doc_wk = frappe.get_doc('Verisae Work Order', wk_name['name'])
        vender_code = doc_wk.vendor if doc_wk.vendor else ""
        procurement_department = doc_wk.procurement_department if doc_wk.procurement_department else ""
        user_wo = doc_wk.user_wo if doc_wk.user_wo else ""
        key = vender_code + "_" + procurement_department + "_" + user_wo
        if not group_by_vendor.get(key): 
            group_by_vendor[key] = [doc_wk]
        else : 
            group_by_vendor[key].append(doc_wk)
    
    for key, list_wk in group_by_vendor.items():
        final_price = 0
        procurement_total_amount = 0
        doc_pd = frappe.new_doc('Verisae Procurement Data')
        for doc_wk in list_wk:
            procurement_total_amount += doc_wk.wo_amount * doc_wk.qty
            doc_pd.append('work_order_reference',{'work_order_name': doc_wk.name, 'amount': doc_wk.wo_amount, 'qty': doc_wk.qty,
                'total_amount': doc_wk.wo_amount * doc_wk.qty, 'work_order': doc_wk.work_order, 'cost_center': doc_wk.cost_center,
                'store_name': doc_wk.store_name, 'format': doc_wk.format, 'oracle_task': doc_wk.oracle_task, 'asset_task': doc_wk.asset_task,
                'fm_admin_email': doc_wk.fm_admin_email, 'description': doc_wk.description, 'tar': doc_wk.tar, 'project_code': doc_wk.project_code_wo,
                'work_order': doc_wk.work_order, 'fm_admin_email': doc_wk.fm_admin_email, 'budget_amount': doc_wk.wo_amount * doc_wk.qty,
            })
        # update args
        doc_pd.vendor_code = doc_wk.vendor
        doc_pd.vendor_name = doc_wk.vendor_name
        doc_pd.final_price = procurement_total_amount
        # doc_pd.total_amount = procurement_total_amount
        doc_pd.procurement_department = doc_wk.procurement_department
        doc_pd.user_procurement = doc_wk.user_wo
        doc_pd.insert(ignore_permissions=True)
    # return doc_pd

