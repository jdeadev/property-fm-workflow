from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.mapper import get_mapped_doc

def execute(filters=None):
    columns, data = get_columns(), get_data(filters)
    return columns, data

def get_data(filters):
    q = frappe.db.sql("""
        select
            pm_work_order_state,
            name,
            vendor_name,
            verisae_sub_category,
            wo_amount,
            fm_approver_level_1,
            pm_reviewer,
            work_order,
            store_name,
            format
        from `tabVerisae Work Order`
        {condition}
    """.format(condition=get_condition(filters)), as_dict=1)
    return q

def get_condition(filters):
    condition = []
    if filters.get('vendor_name'):
        condition.append("vendor_name LIKE '%{0}%' ".format(filters.get('vendor_name')) )
    if filters.get('subcategory'):
        condition.append("verisae_sub_category LIKE '%{0}%' ".format(filters.get('subcategory')) )
    if filters.get('fm_approver_level_1'):
        condition.append("fm_approver_level_1 LIKE '%{0}%' ".format(filters.get('fm_approver_level_1')) )
    if filters.get('pm_reviewer'):
        condition.append("pm_reviewer LIKE '%{0}%' ".format(filters.get('pm_reviewer')) )
    if filters.get('work_order'):
        condition.append("work_order LIKE '%{0}%' ".format(filters.get('work_order')) )
    if filters.get('store_name'):
        condition.append("store_name LIKE '%{0}%' ".format(filters.get('store_name')) )
    if filters.get('format'):
        condition.append("format LIKE '%{0}%' ".format(filters.get('format')) )
    condition.append("pm_work_order_state = 'Wait_Appr2'")
    return "where " + " and ".join(condition) if condition else ""

def get_columns():
    columns = [
        {
            "fieldname": "pm_work_order_state",
            "label": _("PM Work Order State"),
            "fieldtype": "Data",
            "width": 100
        },
        {
            "fieldname": "name",
            "label": _("Name"),
            "fieldtype": "Link",
            "options": "Verisae Work Order",
            "width": 150,
        },
        {
            "fieldname": "vendor_name",
            "label": _("Vendor"),
            "fieldtype": "Data",
            "width": 170
        },
        {
            "fieldname": "verisae_sub_category",
            "label": _("Verisae Sub Category"),
            "fieldtype": "Link",
            "options": "Verisae Sub Category",
            "width": 120,
        },
        {
            "fieldname": "wo_amount",
            "label": _("WO Amount"),
            "fieldtype": "Currency",
            "width": 170
        },
        {
            "fieldname": "fm_approver_level_1",
            "label": _("FM Approver Level 1"),
            "fieldtype": "User",
            "width": 140
        },
        {
            "fieldname": "pm_reviewer",
            "label": _("PM Reviewer"),
            "fieldtype": "User",
            "width": 140
        },
        {
            "fieldname": "work_order",
            "label": _("Work Order"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "store_name",
            "label": _("Store_name"),
            "fieldtype": "Data",
            "width": 100
        },
        {
            "fieldname": "format",
            "label": _("Format"),
            "fieldtype": "Data",
            "width": 100
        },
        
    ]

    return columns

@frappe.whitelist()
def doctype_workflow_action(list_data, action):
    from frappe.model.workflow import apply_workflow
    list_data = frappe.parse_json(list_data)
    for data in list_data:
        apply_workflow(data,action)
    return True
    
