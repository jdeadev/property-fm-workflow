frappe.query_reports["Approver: Verisae Work Order"] = {
    "filters": [
        {
            "fieldname": "vendor_name",
            "label": __("Vendor Name"),
            "fieldtype": "Data",
        },
        {
            "fieldname": "subcategory",
            "label": __("Subcategory"),
            "fieldtype": "Link",
            "options": "Verisae Sub Category",
        },
        {
            "fieldname": "fm_approver_level_1",
            "label": __("FM Approver Appr1"),
            "fieldtype": "Link",
            "options": "User",
        },
        {
            "fieldname": "pm_reviewer",
            "label": __("PM Reviewer"),
            "fieldtype": "Link",
            "options": "User",
        },
        {
            "fieldname": "work_order",
            "label": __("Work Order"),
            "fieldtype": "Data",
        },
        {
            "fieldname": "store_name",
            "label": __("Store Name"),
            "fieldtype": "Data",
        },
        {
            "fieldname": "format",
            "label": __("Format"),
            "fieldtype": "Data",
        },


    ],
    get_datatable_options(options) {
		checkbox_column = Object.assign(options, {
            checkboxColumn: true,
            events: {
				onCheckRow: function(data) {
                }
            }
        });
        return checkbox_column
    },
    onload: function() {
        frappe.query_report.page.add_action_item('Approve', () => action_approve('Approve'))
        frappe.query_report.page.add_action_item('Reject', () => action_approve('Reject'))
    }
 
}

function get_selected() {
    let report = frappe.query_report;
    let checked_rows_indexes = report.datatable.rowmanager.getCheckedRows();
    let checked_rows = checked_rows_indexes.map(i => Object.assign(report.data[i],{'doctype': frappe.query_report.report_doc.ref_doctype}));
    // let checked_rows = checked_rows_indexes.map(i => {
    //     Object.assign(report.data[i],{'doctype': frappe.query_report.report_doc.ref_doctype})
    //     report.data[i].vendor_name = unescape(encodeURIComponent(report.data[i].vendor_name));
    //     report.data[i].verisae_sub_category = unescape(encodeURIComponent(report.data[i].verisae_sub_category));
    //     report.data[i].store_name = unescape(encodeURIComponent(report.data[i].store_name));
    //     return report.data[i]
    // });
    // console.log(checked_rows)
    return checked_rows
}

function action_approve_list(list_data, action){
    return new Promise(resolve => { 
        frappe.call({
            method: 'erpnext.accounts.report.approver:_verisae_work_order.approver:_verisae_work_order.doctype_workflow_action',
            args: {
                list_data: list_data, 
                action: action,
            },
            // freeze the screen until the request is completed
            freeze: true,
            callback: (r) => {
                // frappe.query_report.refresh_report()
            },
            error: (r) => {
                if (r.exc) {
                    frappe.throw(data.name)
                    resolve(false);
                }
            }
        }) 
        resolve(true);
    })
}

async function action_approve(action) {
    let list_data = get_selected()
    let is_error = await action_approve_list(list_data, action);
    location.reload()
}
