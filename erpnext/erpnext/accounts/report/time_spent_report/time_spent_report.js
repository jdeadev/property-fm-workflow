// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Time Spent Report"] = {
    filters: [
        {
            fieldname: "doctype",
            label: __("DocType"),
            fieldtype: "Link",
            options: "DocType",
            reqd: 1,
            get_query: () => {
                return {
                    query: "erpnext.accounts.report.time_spent_report.time_spent_report.get_doctype"
                }
            },
            on_change: function() {
                frappe.query_report.set_filter_value('status', null);
            }
        },
        {
            fieldname: "status",
            label: __("Status"),
            fieldtype: "Link",
            options: "Workflow State",
            reqd: 1,
            get_query: () => {
                return {
                    query: "erpnext.accounts.report.time_spent_report.time_spent_report.get_status",
                    filters: {
                        doctype: frappe.query_report.get_filter_value('doctype')
                    }
                }
            },
        },
        {
            fieldname: "to_status",
            label: __("To Status"),
            fieldtype: "Link",
            options: "Workflow State",
            reqd: 1,
            get_query: () => {
                return {
                    query: "erpnext.accounts.report.time_spent_report.time_spent_report.get_status",
                    filters: {
                        doctype: frappe.query_report.get_filter_value('doctype')
                    }
                }
            },
        },
        {
            fieldname: "date_created",
            label: __("Date Created"),
            fieldtype: "Date",
            reqd: 1,
        },
        {
            fieldname: "date_period",
            label: __("Date Period"),
            // fieldtype: "Select",
            // options: '7\n14\n30',
            default: 1,
            fieldtype: "Int",
            reqd: 1,
        },
    ],
    onload: function() {
        // 2023-12-13 Comment out because p'Dao need to input with int
		// return  frappe.call({
		// 	method: "erpnext.accounts.report.time_spent_report.time_spent_report.get_date_period",
		// 	callback: function(r) {
		// 		var date_period = frappe.query_report.get_filter('date_period');
		// 		date_period.df.options = r.message;
		// 		date_period.df.default = r.message.split("\n")[0];
		// 		date_period.refresh();
		// 		date_period.set_input(date_period.df.default);
		// 	}
		// });
	}
};