# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from itertools import groupby
from frappe.utils import cint

def execute(filters=None):
    columns, data = get_columns(filters), get_data(filters)
    return columns, data

def get_columns(filters):
    doctype_map = frappe.db.get_list('Time Spent Report Details', 
        filters={ 'ref_doctype': filters.get('doctype') }, 
        fields=['workflow_field', 'from_status', 'to_status']
    )
    doctype_map = doctype_map[0]

    return [
        {
            "fieldname": "name",
            "label": _("Document ID"),
            "fieldtype": "Link",
            "options": filters.get('doctype'),
            "width": 150
        },
        {
            "fieldname": "from_status_date",
            "label": _("Status Date"),
            "fieldtype": "Date",
            # "width": 0
        },
        {
            "fieldname": "to_status_date",
            "label": _("Changed Date"),
            "fieldtype": "Date",
            # "width": 0
        },
        # {
        #     "fieldname": "from_status_date",
        #     "label": _("From Status Date"),
        #     "fieldtype": "Date",
        #     # "width": 0
        # },
        # {
        #     "fieldname": "to_status_date",
        #     "label": _("To Status Date"),
        #     "fieldtype": "Date",
        #     # "width": 0
        # },
        {
            "fieldname": "status",
            "label": _("Status"),
            "fieldtype": "Data",
            "width": 200
        },
        {
            "fieldname": "to_status",
            "label": _("To Status"),
            "fieldtype": "Data",
            "width": 200
        },
        {
            "fieldname": "changer",
            "label": _("Changer"),
            "fieldtype": "Data",
            "width": 200
        },
        {
            "fieldname": "date_period",
            "label": _("Date Period"),
            "fieldtype": "Int",
            # "width": 0
        },
        # {
        #     "fieldname": "from_status_date",
        #     "label": _(f"""{doctype_map['from_status']} Date"""),
        #     "fieldtype": "Date",
        #     # "width": 0
        # },
        # {
        #     "fieldname": "to_status_date",
        #     "label": _(f"""{doctype_map['to_status']} Date"""),
        #     "fieldtype": "Date",
        #     # "width": 0
        # },
        # {
        #     "fieldname": "date_diff",
        #     "label": _("Date Diff"),
        #     "fieldtype": "Int",
        #     # "width": 0
        # },
    ]

def get_data(filters):
    doctype = filters.get('doctype')
    status = filters.get('status', 'All')
    to_status = filters.get('to_status', 'All')
    date_period = cint(filters.get('date_period'), 1)
    # date_diff_period = cint(filters.get('date_diff_period'), 0)
    date_created = filters.get("date_created")

    doctype_map = frappe.db.get_list('Time Spent Report Details', 
        filters={ 'ref_doctype': doctype }, 
        fields=['workflow_field', 'from_status', 'to_status']
    )
    doctype_map = doctype_map[0]

    # doc = frappe.db.sql(f"""
    #     select
    #         doc.name,
    #         DATE(doc.creation) 'create_date',
    #         DATE(ver.modified) 'changed_date',
    #         JSON_UNQUOTE(json_extract(ver.data, '$.changed[0][1]')) 'status',
    #         JSON_UNQUOTE(json_extract(ver.data, '$.changed[0][2]')) 'to_status',
    #         ver.owner 'changer',
    #         DATEDIFF(ver.modified, doc.creation) 'date_period'
    #     from `tab{doctype}` doc
    #     left join `tabVersion` ver on doc.name = ver.docname
    #     where ver.ref_doctype = '{doctype}'
    #     and json_extract(ver.data, '$.changed[0][0]') = '{doctype_map['workflow_field']}'
    #     and json_extract(ver.data, '$.changed[0][1]') <> 'null'
    #     order by doc.name asc, ver.modified asc
    # """, as_dict=True)

    doc = frappe.db.sql(
        f"""
        SELECT
            doc.name,
            DATE(doc.creation) 'create_date',
            DATE(ver.modified) 'changed_date',
            version.status,
            version.to_status,
            ver.owner 'changer',
            DATEDIFF(ver.modified, doc.creation) 'date_period'
        FROM
            `tabVersion` ver
            left join `tab{doctype}` doc on ver.docname = doc.name,
            JSON_TABLE(
                json_extract(ver.data, '$.changed'),
                "$[*]" COLUMNS(
                    field VARCHAR(50) PATH '$[0]',
                    status VARCHAR(50) PATH '$[1]',
                    to_status VARCHAR(50) PATH '$[2]'
                )
            ) AS version
        where 
            ver.ref_doctype = '{doctype}'
            and version.field = '{doctype_map['workflow_field']}'
            and doc.creation >= '{date_created} 00:00:00'
            /*and version.status = '{status}'
            and version.to_status = '{to_status}'*/
        order by doc.name asc, ver.modified asc
    """,
        as_dict=True,
    )

    _doc = []
    for key, group in groupby(doc, lambda x: x.name):
        res = { 
            'name': None,
            'create_date': None,
            'changed_date': None,
            'from_status_date': None, 
            'to_status_date': None, 
            'status': None,
            'to_status': None,
            'changer': None,
            'date_period': 0,
        }
        for d in group:
            if not res.get('name') and d.name:
                res.update({ 'name': d.name })
            if not res.get('create_date') and d.create_date:
                res.update({ 'create_date': d.create_date })

            # if status is duplicate set last changed_date
            # if res.get('from_status_date') and d.status == status:
            #     res.update({"status": status, "from_status_date": d.changed_date})

            if (
                res.get("from_status_date")
                and d.status == status
                and d.to_status == to_status
            ):
                res.update({"status": status, "from_status_date": d.changed_date})

            if not res.get('from_status_date') and d.to_status == status:
                res.update({"status": status, "from_status_date": d.changed_date})

            if not res.get('from_status_date') and d.status == status:
                res.update({"status": status, "from_status_date": d.changed_date})

                if d.to_status == to_status:
                    res.update({
                        'status': d.status,
                        'from_status_date': d.create_date,
                        'to_status': d.to_status,
                        'to_status_date': d.changed_date,
                        'changed_date': d.changed_date,
                        'changer': d.changer,
                    })

            if d.to_status == to_status:
                res.update({ 
                    'to_status': d.to_status,
                    'to_status_date': d.changed_date,
                    'changed_date': d.changed_date,
                    'changer': d.changer,
                })

        if res.get('name'):
            date_diff = 0
            if res.get('to_status_date') and res.get('from_status_date'):
                date_diff = frappe.utils.date_diff(
                    res.get('to_status_date'),
                    res.get('from_status_date')
                ) or 1

            res.update({ 'date_period': date_diff })
            _doc.append(res)

    doc = list({ v.get('name'): v for v in _doc }.values())

    doc_filter = []
    for d in doc:
        if d.get('status') == status and d.get('to_status') == to_status:
            if d.get('date_period') >= date_period:
                doc_filter.append(d)

    return doc_filter

@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_doctype(doctype, txt, searchfield, start, page_len, filters, as_dict=False):
    doctype = []
    # ref_doctype = frappe.db.get_list('Time Spent Report Details', 
    #     fields=['ref_doctype']
    # )
    
    # for d in ref_doctype:
    #     doctype.append({ 'name': d.ref_doctype })

    doctype = frappe.db.sql("""
        select ref_doctype 'name', ref_doctype
        from `tabTime Spent Report Details`
    """, as_dict=as_dict)

    return doctype

@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_status(doctype, txt, searchfield, start, page_len, filters, as_dict=False):
    status = []
    # workflow_name = frappe.db.get_value('Workflow', 
    #     { 'document_type': doctype, 'is_active': 1 }, 
    #     ['name']
    # )
 
    # state = frappe.db.get_list('Workflow Document State',
    #     fields=['state'],
    #     filters={ 'parent': workflow_name }
    # )

    # for d in state:
    #     status.append({ 'name': d.state })

    sql = ''
    if filters.get('doctype'):
        sql = f""" and wf.document_type = '{filters.get('doctype')}'"""

    if filters.get('without_closed_cancelled', 0):
        sql += f""" and wds.state not in ('Closed', 'Cancelled')"""

    status = frappe.db.sql(f"""
        select wds.state 'name', wf.document_type
        from `tabWorkflow Document State` wds
        left join `tabWorkflow` wf on wds.parent = wf.name
        where wf.is_active = 1 {sql} and wds.state like '%{txt}%'
        order by wds.idx asc
    """, as_dict=as_dict)

    return status

@frappe.whitelist()
def get_date_period():
    date_period = frappe.db.get_single_value('Time Spent Report Settings', 'date_period')
    return date_period

@frappe.whitelist()
def get_from_to_status(doctype):
    return frappe.db.sql(f"""
        select from_status, to_status
        from `tabTime Spent Report Details`
        where ref_doctype = '{doctype}'
    """, as_dict=True)[0]
