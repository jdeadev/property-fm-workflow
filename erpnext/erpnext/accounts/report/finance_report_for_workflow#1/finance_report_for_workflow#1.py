from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.mapper import get_mapped_doc

def execute(filters=None):
    columns, data = get_columns(), get_data(filters)
    return columns, data

def get_data(filters):
    q = frappe.db.sql("""
        SELECT
            vr.name name,
            vr.date date,
            vr.ppm_workflow_state ppm_workflow_state,
            vr.work_form work_form,
            vr.parent_department department,
            vr.pm_reviewer pm_reviewer,
            vr.project_code project_code,
            vr.project_name project_name,
            vr.account_code account_code,
            vr.item_code item_code,
            vr.vendor_code vendor_code,
            vr.vendor_name vendor_name,
            vr.description description,
            vc.cost_center cost_center,
            vc.store_name store_name,
            vc.format format,
            vr.budget budget,
            vr.total_vendor_amount total_vendor_amount,
            vr.saving_calculation saving_calculation,
            fm.po po_name,
            (select pr from `tabFinance Table` fn where fn.parent = vr.name and fn.po = fm.po limit 1) as pr_name,
            fm.total_amount total_amount,
            fm.receive receive,
            vr.procurement_person_table procurement_person_table,
            vr.procurement_approval_lv_1 procurement_approval_lv_1,
            vr.procurement_approval_lv_2 procurement_approval_lv_2,
            vr.procurement_approval_lv_3 procurement_approval_lv_3,
            vr.procurement_approval_lv_4 procurement_approval_lv_4,
            vr.capex_type capex_type,
            vr.verisae_category verisae_category,
            vr.verisae_sub_category verisae_sub_category,
            vr.warranty warranty,
            vr.days days,
            vr.ppm_service ppm_service,
            vr.times times,
            vr.month month,
            vr.year year,
            vr.write_off write_off,
            vr.project_completed_date as project_completed_date
        FROM `tabNon Verisae Request` vr
        LEFT JOIN `tabNon Verisae Cost Center` vc on vc.parent = vr.name
        LEFT JOIN `tabFM PO Table` fm on fm.parent = vr.name
        LEFT JOIN `tabFinance Table` fn on fn.parent = vr.name
        {conditions}
    """.format(conditions=get_condition(filters)), as_dict=1)
    return q

def get_condition(filters):
    condition = []
    if "Lotus Confidential" in frappe.get_roles() and frappe.session.user != 'Administrator':
        condition.append("vr.owner = '{user}'".format(user=frappe.session.user))
        condition.append("vr.pm_reviewer = '{user}'".format(user=frappe.session.user))
        condition.append("vr.fm_approver_level_1 = '{user}'".format(user=frappe.session.user))
    return "WHERE " + " OR ".join(condition) if condition else ""

def get_columns():
    columns = [
        {
            'fieldname': 'name',
            'label': _('ID'),
            'fieldtype': 'Link',
            'options': 'Non Verisae Request',
        },
        {
            'fieldname': 'date',
            'label': _('Date'),
            'fieldtype': 'Date',
            'width': 100,
        },
        {
            'fieldname': 'ppm_workflow_state',
            'label': 'PPM WorkFlow State',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'work_form',
            'label': 'Work Form',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'department',
            'label': _('Department'),
            'fieldtype': 'Link',
            'options': 'Department',
        },
        {
            'fieldname': 'pm_reviewer',
            'label': _('PM Reviewer'),
            'fieldtype': 'Link',
            'options': 'User',
        },
        {
            'fieldname': 'project_code',
            'label': _('Project Code'),
            'fieldtype': 'Link',
            'options': 'Project',
        },
        {
            'fieldname': 'project_name',
            'label': _('Project Name'),
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'account_code',
            'label': _('Account Code'),
            'fieldtype': 'Link',
            'options': 'Account FM',
        },
        {
            'fieldname': 'item_code',
            'label': _('Item Code'),
            'fieldtype': 'Link',
            'options': 'Account FM',
        },
        {
            'fieldname': 'vendor_code',
            'label': _('Vendor Code'),
            'fieldtype': 'Link',
            'options': 'Vendor',
        },
        {
            'fieldname': 'vendor_name',
            'label': _('Vendor Name'),
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'description',
            'label': 'Description',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'cost_center',
            'label': _('Cost Center (Cost Center)'),
            'fieldtype': 'Link',
            'options': 'Store Active',
        },
        {
            'fieldname': 'store_name',
            'label': 'Store Name (Cost Center)',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'format',
            'label': 'Format (Cost Center)',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'budget',
            'label': _('Total Budget (Exclude VAT)'),
            'fieldtype': 'Currency',
        },
        {
            'fieldname': 'total_amount',
            'label': _('Total Amount'),
            'fieldtype': 'Currency',
        },
        {
            'fieldname': 'saving_calculation',
            'label': _('Saving Calculation'),
            'fieldtype': 'Currency',
        },
        {
            'fieldname': 'pr_name',
            'label': 'PR (Finance Table)',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'po_name',
            'label': 'PO (FM PO Table)',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'total_amount_po',
            'label': _('Total Amount (FM PO Table)'),
            'fieldtype': 'Currency',
        },
        {
            'fieldname': 'receive_amount_po',
            'label': _('Receive (FM PO Table)'),
            'fieldtype': 'Currency',
        },
        {
            'fieldname': 'procurement_person_assigned',
            'label': _('Procurement Person Assigned'),
            'fieldtype': 'Link',
            'options': 'User',
        },
        {
            'fieldname': 'procurement_approval_lv_1',
            'label': _('Procurement Approval Lv 1'),
            'fieldtype': 'Link',
            'options': 'User',
        },
        {
            'fieldname': 'procurement_approval_lv_2',
            'label': _('Procurement Approval Lv 2'),
            'fieldtype': 'Link',
            'options': 'User',
        },
        {
            'fieldname': 'procurement_approval_lv_3',
            'label': _('Procurement Approval Lv 3'),
            'fieldtype': 'Link',
            'options': 'User',
        },
        {
            'fieldname': 'procurement_approval_lv_4',
            'label': _('Procurement Approval Lv 4'),
            'fieldtype': 'Link',
            'options': 'User',
        },
        {
            'fieldname': 'capex_type',
            'label': 'Capex Type',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'verisae_category',
            'label': _('Verisae Category'),
            'fieldtype': 'Link',
            'options': 'Verisae category',
        },
        {
            'fieldname': 'verisae_sub_category',
            'label': _('Verisae Sub Category'),
            'fieldtype': 'Link',
            'options': 'Verisae Sub Category',
        },
        {
            'fieldname': 'warranty',
            'label': 'Warranty',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'days',
            'label': 'Warranty Period (Days)',
            'fieldtype': 'Int',
        },
        {
            'fieldname': 'ppm_service',
            'label': 'PPM Service',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'times',
            'label': 'Full Contract PPM Service (Times)',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'month',
            'label': 'Full Contract  PPM Service Period (Months)',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'year',
            'label': 'Year',
            'fieldtype': 'Int',
        },
        {
            'fieldname': 'write_off',
            'label': 'Write off',
            'fieldtype': 'Data',
        },
        {
            'fieldname': 'project_completed_date',
            'label': 'Project Completed Date',
            'fieldtype': 'Date',
            'width': 100,
        },
    ]

    return columns