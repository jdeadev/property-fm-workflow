from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.mapper import get_mapped_doc

def execute(filters=None):
    columns, data = get_columns(), get_data(filters)
    return columns, data

def get_data(filters):
    q = frappe.db.sql("""
        SELECT
			vp.name ,
			vp.procurement_data_state ,
			vw.parent_department ,
			vw.pm_reviewer ,
			vw.project_code_wo ,
            vw.item_code ,
            (select item_name from `tabItem` where name = vw.item_code) as item_name ,
			vp.vendor_code ,
            vp.vendor_name ,
			vw.work_order ,
			wo.work_order_name ,
			wo.cost_center ,
			wo.store_name ,
			wo.format ,
			vp.final_price ,
			vp.saving_calculation ,
            ft.pr ,
			fm.po ,
			fm.total_amount ,
			fm.receive ,
			vp.user_procurement ,
			vp.procurement_approver_lv_1 ,
			vp.procurement_approver_lv_2 ,
			vp.procurement_approver_lv_3 ,
			vp.procurement_approver_lv_4 
		FROM `tabVerisae Procurement Data` vp
		LEFT JOIN `tabWO Reference` wo on wo.parent = vp.name
        LEFT JOIN `tabFinance Table` ft on ft.parent = vp.name
		LEFT JOIN `tabFM PO Table` fm on fm.parent = vp.name
		LEFT JOIN `tabVerisae Work Order` vw on vw.name = wo.work_order_name
    """)
    return q

# def get_condition(filters):
#     condition = []
#     if filters.get('vendor_code'):
#         condition.append("vendor LIKE '%{0}%' ".format(filters.get('vendor_code')) )
#     return " and ".join(condition) if condition else ""

def get_columns():
    columns = [
        "Verisae Procurement Data:Link/Verisae Procurement Data",
		"PPM WorkFlow State",
		"Department:Link/Department",
		"PM Reviewer:Link/User",
		"Project Code:Link/Project",
        "Item Code:Link/Item",
        "Item Name",
		"Vendor Code:Link/Vendor",
        "Vendor Name",
		"Work Order:Data",
		"Verisae Work Order:Link/Verisae Work Order",
		"Cost Center:Link/Store Active",
		"Store Name",
		"Format",
		"Final Price:Currency:150",
		"Saving Calculation:Currency",
        "PR (Finance Table)",
		"PO (FM PO Table)",
		"Total Amount (FM PO Table):Currency",
		"Receive (FM PO Table):Currency",
		"Procurement Person:Link/User",
		"Procurement Approver Lv 1:Link/User",
		"Procurement Approver Lv 2:Link/User",
		"Procurement Approver Lv 3:Link/User",
		"Procurement Approver Lv 4:Link/User",
    ]

    return columns

