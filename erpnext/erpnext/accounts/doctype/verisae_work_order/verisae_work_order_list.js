function set_base_list() {
    frappe.views.BaseList.prototype.get_call_args = function () {
        const args = this.get_args();
        args.or_filters = [];
        
        if (frappe.user.has_role('Lotus Confidential') && frappe.session.user != 'Administrator') {
            args.or_filters.push(['Verisae Work Order', 'owner', '=', frappe.session.user]);
            args.or_filters.push(['Verisae Work Order', 'pm_reviewer', '=', frappe.session.user]);
            args.or_filters.push(['Verisae Work Order', 'fm_approver_level_1', '=', frappe.session.user]);
        }

        return {
            method: this.method,
            args: args,
            freeze: this.freeze_on_refresh || false,
            freeze_message: this.freeze_message || (__('Loading') + '...')
        };
    }
}

set_base_list();

frappe.listview_settings['Verisae Work Order'] = {
    //filters: [['assign_to', '=', frappe.session.user]],
    refresh: function (listview) {
        set_base_list();
    },
};
