# Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class VerisaeWorkOrder(Document):
	def onload(self):
		self.lotus_confidential()

	def lotus_confidential(self):
		if ('Lotus Confidential' in frappe.get_roles() and frappe.session.user != 'Administrator'):
			if not (self.owner == frappe.session.user or self.pm_reviewer == frappe.session.user or self.fm_approver_level_1 == frappe.session.user):
				frappe.msgprint(
					msg='You do not have permission to access the document.',
					title='Permission Error',
					raise_exception=1,
				)
