# -*- coding: utf-8 -*-
# Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class VerisaeProcurementData(Document):
    def after_insert(self):
        if self.work_order_reference:
            for wo in self.work_order_reference:
                doc_wo = frappe.get_doc('Verisae Work Order', wo.work_order_name)
                doc_wo.related_to_procurement_doc_id = self.name
                # doc_wo.save()
                if doc_wo.pm_work_order_state == 'Approved':
                    doc_wo.pm_work_order_state = 'Converted'
                    doc_wo.flags.ignore_permissions = True
                    doc_wo.save(ignore_permissions=True)

    def validate(self):
        if self.procurement_data_state == 'Cancelled':
            if self.work_order_reference:
                for wo in self.work_order_reference:
                    frappe.db.set_value('Verisae Work Order', wo.work_order_name, {
                        'pm_work_order_state': "Approved",
                        'docstatus': 0,
                    })
        elif self.procurement_data_state == 'Closed':
            if self.work_order_reference:
                for wo in self.work_order_reference:
                    frappe.db.set_value('Verisae Work Order', wo.work_order_name, {
                        'pm_work_order_state': "Closed",
                        'docstatus': 0,
                    })

    def on_cancel(self):
        if self.work_order_reference:
            for wo in self.work_order_reference:
                doc_wo = frappe.get_doc('Verisae Work Order', wo.work_order_name)
                doc_wo.pm_work_order_state = "Approved"
                doc_wo.docstatus = 0
                doc_wo.save(ignore_permissions=True)
