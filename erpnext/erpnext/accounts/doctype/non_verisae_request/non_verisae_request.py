# Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class NonVerisaeRequest(Document):
	def validate(self):
		pass
		
	def onload(self):
		self.lotus_confidential()

	def lotus_confidential(self):
		if ('Lotus Confidential' in frappe.get_roles() and frappe.session.user != 'Administrator'):
			if not (self.owner == frappe.session.user or self.pm_reviewer == frappe.session.user or self.fm_approver_level_1 == frappe.session.user):
				frappe.msgprint(
					msg='You do not have permission to access the document.',
					title='Permission Error',
					raise_exception=1,
				)

def update_link_file():
	doctype = 'Non Verisae Request'
	get_all_doc = frappe.get_all(doctype)
	get_meta_doctype = frappe.get_meta(doctype)
	list_field_attach = [field.fieldname for field in get_meta_doctype.get('fields', {'fieldtype': 'Attach'})]
	for i in get_all_doc:
		get_doc = frappe.get_doc(doctype, i.name)
		files_count = {}
		for field in list_field_attach:
			if not get_doc.get(field): continue
			if not files_count.get(get_doc.get(field)):
				files_count[get_doc.get(field)] = 1
			else:
				files_count[get_doc.get(field)] += 1
		for doc_file_url in files_count:
			count_files_ref_doc = frappe.db.count('File', {'attached_to_name': i.name, 'file_url': doc_file_url})
			if count_files_ref_doc < files_count.get(doc_file_url):
				if frappe.db.count('File', {'attached_to_name': ['like', '%new%'], 'file_url': doc_file_url}) > 0:
					get_file_doc = frappe.get_doc('File', {'attached_to_name': ['like', '%new%'], 'file_url': doc_file_url})
					get_file_doc.attached_to_name = i.name
					get_file_doc.save()