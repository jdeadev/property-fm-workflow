import frappe
from frappe import _

@frappe.whitelist()
def sign_up(data):
    get_data = frappe.parse_json(data)
    # Validate Data
    validata_data(get_data)
    # Validate Password
    if get_data.password != get_data.con_password:
        frappe.throw(
            title= _('Password invalid'),
            msg=_("Password doesn't match"),
        )
    get_user = {
        'doctype': 'User',
        'first_name': get_data.first_name, 
		'last_name': get_data.last_name,
		'email': get_data.email,
		'new_password': get_data.password,
		'user_type': 'System User',
		'role_profile_name': get_data.role_profile_name,
        'department': get_data.department,
    }
    # Create User
    new_user = frappe.get_doc(get_user)
    new_user.insert()
    return new_user

def validata_data(get_data):
    # field mandatory (data is not null)
    msg = ""
    error_fields = {
		'first_name': 'First Name', 
		'last_name': 'Last Name',
		'email': 'Email',
		'password': 'Password',
		'con_password': 'Confirm Password',
		'user_type': 'User Type',
		'role_profile_name': 'Role Profile',
        'department': 'Deparment'
	}
    for key, value in get_data.items():
        if not value:
            msg += "<li>" + error_fields[key] + "</li>"
    if msg:
        return  frappe.throw(
                    title= _('Missing Values Required'),
                    msg= _('Following fields have missing values:') +
                        '<br><br><ul>' + msg + '</ul>',
                    # indicator= 'orange'
                )