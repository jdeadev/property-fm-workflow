frappe.pages['user-register'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'User Register',
		single_column: true
	});
	
	$(frappe.render_template("user_register", this)).appendTo(this.page.main);

	var selectDepartment = document.getElementById('userDepartment');
	frappe.db.get_list('Department').then(element => {
		element.forEach(doc => {
			var opt = document.createElement('option');
			opt.value = doc.name;
			opt.innerHTML = doc.name;
			selectDepartment.appendChild(opt);
		});
	});

	// var selectUserType = document.getElementById('userType');
	// frappe.db.get_list('User Type').then(element => {
	// 	element.forEach(doc => {
	// 		var opt = document.createElement('option');
	// 		opt.value = doc.name;
	// 		opt.innerHTML = doc.name;
	// 		selectUserType.appendChild(opt);
	// 	});
	// });

	var selectRoleProfile = document.getElementById('roleProfile');
	frappe.db.get_list('Role Profile', { limit: 0 }).then(element => {
		element.forEach(doc => {
			var opt = document.createElement('option');
			opt.value = doc.name;
			opt.innerHTML = doc.name;
			selectRoleProfile.appendChild(opt);
		});
	});
}


function sign_in() {
	var firstName = document.getElementById('firstName').value;
	var lastName = document.getElementById('lastName').value;
	var inputEmail = document.getElementById('inputEmail').value;
	var inputPassword = document.getElementById('inputPassword').value;
	var inputConPassword = document.getElementById('inputConPassword').value;
	// var userType = document.getElementById('userType').value;
	var roleProfile = document.getElementById('roleProfile').value;
	var userDepartment = document.getElementById('userDepartment').value;
	let data = {
		'first_name': firstName, 
		'last_name': lastName,
		'email': inputEmail,
		'password': inputPassword,
		'con_password': inputConPassword,
		// 'user_type': userType,
		'role_profile_name': roleProfile,
		'department': userDepartment,
	}
	frappe.call({
		method: "frappe.core.page.user_register.user_register.sign_up",
		args: {'data': data},
		freeze: true,
		callback: function (r) {
			frappe.msgprint("Sign in successly");
		}
	});
}
